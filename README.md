# gdriveviewonlydownloader

Go-based command-line utility to download view-only documents on Google Drive to a PDF file.

Install with `go get -v gitlab.com/Lomanic/gdriveviewonlydownloader`

```
Usage of gdrivedownloader:
  -pdf string
        output PDF (default "/tmp/FILE_ID/pdf.pdf")
  -resume
        resume download of pages instead of starting over from scratch
  -url string
        view-only gdrive document to download as PDF (default "https://drive.google.com/file/d/FILE_ID/view")
  -width int
        width of the downloaded PNGs from which the pages are generated. -1 for downloading the biggest width available (default 1000)
  -workdir string
        directory to which PNGs are downloaded (default "/tmp/FILE_ID")
```