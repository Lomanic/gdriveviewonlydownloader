package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/jung-kurt/gofpdf"
)

var downloadURL string
var pngWidth int
var workDir string
var outPDF string
var resumeDownload bool

const defaultURL string = "https://drive.google.com/file/d/FILE_ID/view"

var defaultWorkDir string = filepath.Join(os.TempDir(), strings.Split(defaultURL, "/")[5])
var defaultPDF string = filepath.Join(defaultWorkDir, "pdf.pdf")

const defaultPNGWidth = 1000

func init() {
	flag.StringVar(&downloadURL, "url", defaultURL, "view-only gdrive document to download as PDF")
	flag.IntVar(&pngWidth, "width", defaultPNGWidth, "width of the downloaded PNGs from which the pages are generated. -1 for downloading the biggest width available")
	flag.StringVar(&workDir, "workdir", defaultWorkDir, "directory to which PNGs are downloaded")
	flag.StringVar(&outPDF, "pdf", defaultPDF, "output PDF")
	flag.BoolVar(&resumeDownload, "resume", false, "resume download of pages instead of starting over from scratch")
	flag.Parse()
	splittedURL := strings.Split(downloadURL, "/")
	if len(splittedURL) > 5 && workDir == defaultWorkDir {
		workDir = filepath.Join(os.TempDir(), splittedURL[5])
	}
	if outPDF == defaultPDF {
		outPDF = filepath.Join(workDir, "pdf.pdf")
	}
	if downloadURL == defaultURL {
		flag.PrintDefaults()
		os.Exit(1)
	}
}

type viewerngUploadJSON struct {
	Meta      string `json:"meta"`
	Status    string `json:"status"`
	Page      string `json:"page"`
	Presspage string `json:"presspage"`
	Press     string `json:"press"`
	Img       string `json:"img"`
}

type viewerngMetaJSON struct {
	Pages        int `json:"pages"`
	MaxPageWidth int `json:"maxPageWidth"`
}

func main() {
	res, err := http.Get(downloadURL)
	if err != nil {
		log.Fatal(err)
	}
	gdriveDoc, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
	// congrats, we have now the whole gdrive gdriveDocument. Let's find out the JSON upload viewerng page
	re := regexp.MustCompile(`"https://drive\.google\.com/viewerng/upload\?.+?"`)
	viewerngUploadMatches := re.FindSubmatch(gdriveDoc)
	if len(viewerngUploadMatches) == 0 {
		log.Fatal("unable to extract viewerng upload URL from this gdrive document")
	}
	viewerngUploadURL, err := strconv.Unquote(string(viewerngUploadMatches[0]))
	if err != nil {
		log.Fatal(err)
	}

	res, err = http.Get(viewerngUploadURL)
	if err != nil {
		log.Fatal(err)
	}
	viewerngUploadDoc, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}

	var viewerngUploadData viewerngUploadJSON
	for i := 0; i < len(viewerngUploadDoc); i++ {
		err = json.Unmarshal(viewerngUploadDoc[i:], &viewerngUploadData)
		if err == nil {
			break
		}
	}
	if err != nil || viewerngUploadData.Img == "" {
		log.Fatal("unable to parse JSON from viewerng upload page")
	}

	// get number of pages and max width through viewerng meta page
	viewerngMetaURL := fmt.Sprintf("https://drive.google.com/viewerng/%s", viewerngUploadData.Meta)
	res, err = http.Get(viewerngMetaURL)
	if err != nil {
		log.Fatal(err)
	}
	viewerngMetaDoc, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}

	var viewerngMetaData viewerngMetaJSON
	for i := 0; i < len(viewerngMetaDoc); i++ {
		err = json.Unmarshal(viewerngMetaDoc[i:], &viewerngMetaData)
		if err == nil {
			break
		}
	}
	if err != nil || viewerngMetaData.Pages == 0 {
		log.Fatal("unable to parse JSON from viewerng meta page")
	}

	if pngWidth <= 0 {
		pngWidth = viewerngMetaData.MaxPageWidth
	}

	if resumeDownload {
		downloadPagesToPNGs(viewerngUploadData, viewerngMetaData, workDir)
		generatePDFFromWorkDir(viewerngMetaData.Pages, workDir, outPDF)
	} else {
		downloadPagesAndGeneratePDFFromMemory(viewerngUploadData, viewerngMetaData, outPDF)
	}

	// TODO: if possible, restore text information from fmt.Sprintf("https://drive.google.com/viewerng/%s", viewerngUploadData.Press), it returns a "pdf2xml" XML file (this file is downloaded when initiating a search)
}

func downloadPagesAndGeneratePDFFromMemory(viewerngUploadData viewerngUploadJSON, viewerngMetaData viewerngMetaJSON, out string) {
	// download all png files as if we were scrolling through the document
	err := os.MkdirAll(filepath.Dir(out), 0755)
	if err != nil {
		log.Fatal("unable to create workdir:", err)
	}
	pdf := gofpdf.New("P", "mm", "A4", "")
	for i := 0; i < viewerngMetaData.Pages; i++ {
		pngURL := fmt.Sprintf("https://drive.google.com/viewerng/%s&page=%d&skiphighlight=true&w=%d", viewerngUploadData.Img, i, pngWidth)
		pngFilePath := fmt.Sprintf("%.*d.png", len(strconv.Itoa(viewerngMetaData.Pages)), i)
		fmt.Println(pngURL)
		res, err := http.Get(pngURL)
		if err != nil {
			log.Fatal(err)
		}
		if res.StatusCode == 400 {
			break
		}
		if res.StatusCode == 500 { // probably rate limited, retry
			time.Sleep(10 * time.Second)
			continue
		}
		if res.StatusCode != 200 {
			log.Fatal("unknown status code for", pngURL, res.StatusCode)
		}

		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("adding page %s\n", pngFilePath)
		pdf.RegisterImageOptionsReader(pngFilePath, gofpdf.ImageOptions{ImageType: "png"}, res.Body)
		pdf.AddPage()
		pageWidth, _ := pdf.GetPageSize()
		pdf.ImageOptions(pngFilePath, 0, 0, pageWidth, 0, false, gofpdf.ImageOptions{ImageType: "png"}, 0, "")
		res.Body.Close()
	}
	err = pdf.OutputFileAndClose(out)
	if err != nil {
		log.Fatal(err)
	}
	// TODO: if possible, restore text information from fmt.Sprintf("https://drive.google.com/viewerng/%s", viewerngUploadData.Press), it returns a "pdf2xml" XML file (this file is downloaded when initiating a search)
}

func downloadPagesToPNGs(viewerngUploadData viewerngUploadJSON, viewerngMetaData viewerngMetaJSON, workDir string) {
	// download all png files as if we were scrolling through the document
	err := os.MkdirAll(workDir, 0755)
	if err != nil {
		log.Fatal("unable to create workdir:", err)
	}
	for i := 0; i < viewerngMetaData.Pages; i++ {
		pngURL := fmt.Sprintf("https://drive.google.com/viewerng/%s&page=%d&skiphighlight=true&w=%d", viewerngUploadData.Img, i, pngWidth)
		pngFilePath := filepath.Join(workDir, fmt.Sprintf("%.*d.png", len(strconv.Itoa(viewerngMetaData.Pages)), i))
		if resumeDownload {
			if _, err := os.Stat(pngFilePath); err == nil {
				continue
			}
		}
		fmt.Println(pngURL)
		res, err := http.Get(pngURL)
		if err != nil {
			log.Fatal(err)
		}
		if res.StatusCode == 400 {
			break
		}
		if res.StatusCode == 500 { // probably rate limited, retry
			time.Sleep(10 * time.Second)
			continue
		}
		if res.StatusCode != 200 {
			log.Fatal("unknown status code for", pngURL, res.StatusCode)
		}

		fmt.Printf("writing %s\n", pngFilePath)
		out, err := os.Create(pngFilePath)
		if err != nil {
			log.Fatal(err)
		}
		_, err = io.Copy(out, res.Body)
		if err != nil {
			log.Fatal(err)
		}
		res.Body.Close()
		out.Close()
	}
}

func generatePDFFromWorkDir(pages int, workDir, out string) {
	pdf := gofpdf.New("P", "mm", "A4", "")
	for i := 0; i < pages; i++ {
		pngFilePath := filepath.Join(workDir, fmt.Sprintf("%.*d.png", len(strconv.Itoa(pages)), i))
		pdf.AddPage()
		//fmt.Println(pdf.GetPageSize()) // 210.00155555555557 297.00008333333335
		//var size gofpdf.SizeType
		//pdf.AddPageFormat("P", size.ScaleToWidth(pixelsToMM(1000)))
		//fmt.Println(pdf.GetPageSize()) // 210.00155555555557 297.00008333333335
		pageWidth, _ := pdf.GetPageSize()
		//pdf.ImageOptions(pngFilePath, 0, 0, -1, -1, false, gofpdf.ImageOptions{ImageType: "png"}, 0, "")
		//pdf.ImageOptions(pngFilePath, 0, 0, 210, 0, false, gofpdf.ImageOptions{ImageType: "png"}, 0, "")
		// insert png while adjusting its width to the current page size
		pdf.ImageOptions(pngFilePath, 0, 0, pageWidth, 0, false, gofpdf.ImageOptions{ImageType: "png"}, 0, "")
	}
	err := pdf.OutputFileAndClose(out)
	if err != nil {
		log.Fatal(err)
	}
}
